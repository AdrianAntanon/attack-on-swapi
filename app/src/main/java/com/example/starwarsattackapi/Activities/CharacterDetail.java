package com.example.starwarsattackapi.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.starwarsattackapi.Models.Character;
import com.example.starwarsattackapi.Models.Film;
import com.example.starwarsattackapi.R;
import com.example.starwarsattackapi.WebServices.WebServiceClient;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CharacterDetail extends AppCompatActivity {

    private List<String> listOfFilms;
    private Film film;
    private int counter;

    private TextView title, description;
    private Button previousButton, nextButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_detail);

        title = findViewById(R.id.charDetailTitle);
        description = findViewById(R.id.charDetailDescription);
        previousButton = findViewById(R.id.previousButtonDetail);
        nextButton = findViewById(R.id.nextButtonDetail);


        title.setVisibility(View.INVISIBLE);
        description.setVisibility(View.INVISIBLE);
        previousButton.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);

        counter = 0;
        previousButton.setOnClickListener(v -> {
            counter--;
            sendRequest();
        });

        nextButton.setOnClickListener(v -> {
            counter++;
            sendRequest();
        });

        Character character = (Character) getIntent().getSerializableExtra("character");
        listOfFilms = character.getFilms();

        sendRequest();
    }

    private void sendRequest(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://swapi.dev/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        WebServiceClient client = retrofit.create(WebServiceClient.class);
        Call<Film> call = client.getFilm(listOfFilms.get(counter));

        call.enqueue(new Callback<Film>() {
            @Override
            public void onResponse(@NotNull Call<Film> call, @NotNull Response<Film> response) {

                film = response.body();

                title.setText(film.getTitle());
                description.setText(film.getOpeningCrawl());

                title.setVisibility(View.VISIBLE);
                description.setVisibility(View.VISIBLE);

                if (counter == 0){
                    previousButton.setVisibility(View.INVISIBLE);
                }else {
                    previousButton.setVisibility(View.VISIBLE);
                }

                int amountOfFilms = listOfFilms.size()-1;

                if (counter == amountOfFilms){
                    nextButton.setVisibility(View.INVISIBLE);
                }else {
                    nextButton.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(@NotNull Call<Film> call, @NotNull Throwable t) {
                Log.d("TAG1", "Error: " + t.getMessage());
            }
        });

    }
}
