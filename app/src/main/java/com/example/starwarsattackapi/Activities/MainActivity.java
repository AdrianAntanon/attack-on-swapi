package com.example.starwarsattackapi.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.starwarsattackapi.Adapter.CharacterAdapter;
import com.example.starwarsattackapi.Models.Character;
import com.example.starwarsattackapi.Models.Data;
import com.example.starwarsattackapi.R;
import com.example.starwarsattackapi.WebServices.WebServiceClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private String nextText, previousText;
    private Button nextButton, previousButton;

    private EditText etSearcher;

    private CharacterAdapter adapter;

    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etSearcher = findViewById(R.id.searcher);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        List<Character> characters = new ArrayList<>();

        ImageButton searcherImageButton = findViewById(R.id.searchButton);
        nextButton = findViewById(R.id.nextButtonMain);
        previousButton = findViewById(R.id.previousButtonMain);

        searcherImageButton.setOnClickListener(v -> {
            String searcherText = etSearcher.getText().toString();
            sendSearchedRequest(searcherText);
        });
        nextButton.setOnClickListener(v -> {
            goBackOrForward(true); //Avanzamos a la siguiente lista de personajes
        });
        previousButton.setOnClickListener(v -> {
            goBackOrForward(false); // Vemos la lista anterior de personajes, solo disponible si hemos avanzado al menos una vez
        });

        adapter = new CharacterAdapter(characters, position -> {
            Intent intent = new Intent(MainActivity.this, CharacterDetail.class);
            intent.putExtra("character", adapter.getCharacters().get(position));
            startActivity(intent);
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        sendRequest();

    }

    private void sendRequest() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder().baseUrl("https://swapi.dev/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        WebServiceClient client = retrofit.create(WebServiceClient.class);

        Call<Data> call = client.getAllCharacters();
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NotNull Call<Data> call, @NotNull Response<Data> response) {
                if (response.body().getPrevious() == null) {
                    previousButton.setVisibility(View.INVISIBLE);
                }
                if (response.body().getNext() != null) {
                    nextText = response.body().getNext();
                } else {
                    nextButton.setVisibility(View.INVISIBLE);
                }

                adapter.setCharacters(response.body().getResults());
            }

            @Override
            public void onFailure(@NotNull Call<Data> call, Throwable t) {
                Log.d("TAG1", "Error: " + t.getMessage());
            }
        });
    }

    private void goBackOrForward(boolean advance) {
        WebServiceClient client = retrofit.create(WebServiceClient.class);
        Call<Data> call;

        if (advance){
            call = client.getNextOrPreviousPage(nextText.split("http://swapi.dev/api/")[1]);
        }else{
            call = client.getNextOrPreviousPage(previousText.split("http://swapi.dev/api/")[1]);
        }


        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NotNull Call<Data> call, @NotNull Response<Data> response) {
                if (response.body().getPrevious() == null) {
                    previousButton.setVisibility(View.INVISIBLE);
                } else {
                    previousButton.setVisibility(View.VISIBLE);
                    previousText = response.body().getPrevious();
                }

                if (response.body().getNext() != null) {
                    nextButton.setVisibility(View.VISIBLE);
                    nextText = response.body().getNext();
                } else {
                    nextButton.setVisibility(View.INVISIBLE);
                }
                adapter.setCharacters(response.body().getResults());

            }

            @Override
            public void onFailure(@NotNull Call<Data> call, Throwable t) {
                Log.d("TAG1", "Error: " + t.getMessage());
            }
        });
    }

    private void sendSearchedRequest(String searcherText) {
        WebServiceClient client = retrofit.create(WebServiceClient.class);
        Call<Data> call = client.getSearchedCharacters("people/?search=" + searcherText);

        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NotNull Call<Data> call, @NotNull Response<Data> response) {
                if (response.body().getPrevious() == null) {
                    previousButton.setVisibility(View.INVISIBLE);
                } else {
                    previousButton.setVisibility(View.VISIBLE);
                    previousText = response.body().getPrevious();
                }

                if (response.body().getNext() != null) {
                    nextButton.setVisibility(View.VISIBLE);
                    nextText = response.body().getNext();
                } else {
                    nextButton.setVisibility(View.INVISIBLE);
                }
                adapter.setCharacters(response.body().getResults());
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                Log.d("TAG1", "Error: " + t.getMessage());
            }
        });
    }

}