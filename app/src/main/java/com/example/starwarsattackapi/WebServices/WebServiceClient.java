package com.example.starwarsattackapi.WebServices;

import com.example.starwarsattackapi.Models.Data;
import com.example.starwarsattackapi.Models.Film;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface WebServiceClient {
    @GET("people")
    Call<Data> getAllCharacters();

    @GET()
    Call<Film> getFilm(@Url String url);

    @GET()
    Call<Data> getSearchedCharacters(@Url String url);

    @GET()
    Call<Data> getNextOrPreviousPage(@Url String url);
}
