package com.example.starwarsattackapi.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.starwarsattackapi.Models.Character;
import com.example.starwarsattackapi.R;

import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterAdapterHolder> {
    private List<Character> characters;
    private OnItemClickListener itemClickListener;

    public CharacterAdapter(List<Character> characters, OnItemClickListener itemClickListener) {
        this.characters = characters;
        this.itemClickListener = itemClickListener;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CharacterAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view,parent,false);

        return new CharacterAdapterHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterAdapterHolder holder, int position) {
        holder.tvName.setText(characters.get(position).getName());
        holder.tvHeight.setText(characters.get(position).getHeight());
        holder.tvEyesColor.setText(characters.get(position).getEye_color());
        holder.tvBirthDate.setText(characters.get(position).getBirth_year());
        holder.bind(this.itemClickListener);
    }

    @Override
    public int getItemCount() {
        return characters.size();
    }


    public class CharacterAdapterHolder extends RecyclerView.ViewHolder{
        TextView tvName, tvHeight, tvEyesColor, tvBirthDate;

        public CharacterAdapterHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.infoNameTV);
            tvHeight = itemView.findViewById(R.id.infoCharHeightTV);
            tvEyesColor = itemView.findViewById(R.id.infoCharEyesColor);
            tvBirthDate = itemView.findViewById(R.id.infoCharBirthDateTV);
        }

        public void bind(final OnItemClickListener clickListener){
            itemView.setOnClickListener(v -> clickListener.onItemClick(getAdapterPosition()));
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
